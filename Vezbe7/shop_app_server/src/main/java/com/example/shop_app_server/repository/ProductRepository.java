package com.example.shop_app_server.repository;

import com.example.shop_app_server.model.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ProductRepository {

    public static ArrayList<Product> products = new ArrayList<>(List.of(
            new Product(1L, "Samsung", "Samsung Galaxy S10", "samsung_galaxy_s23_ultra"),
            new Product(2L, "Samsung", "Samsung Galaxy S20", "samsung_galaxy_s23_ultra_green"),
            new Product(3L, "Iphone", "Iphone 15 pro", "samsung_galaxy_s23_ultra")));

    public List<Product> getAll() {
        // Print the updated list of products
        products.forEach(System.out::println);
        return products;
    }

    public Product getById(Long id){
        List<Product> filteredProducts = products.stream()
                .filter(product -> product.getId().equals(id))
                .collect(Collectors.toList());
        if(filteredProducts != null){
            // Print the filtered products
            filteredProducts.forEach(System.out::println);
            return filteredProducts.get(0);
        }else{
            throw new RuntimeException("Error");
        }
    }

    public Product add(Product product){
        Long id = (long) (products.size() + 1);
        product.setId(id);
        products.add(product);
        // Print the updated list of products
        products.forEach(System.out::println);
        return product;
    }
    public Boolean deleteById(Long id){
        products.removeIf(product -> product.getId().equals(id));
        // Print the updated list of products
        products.forEach(System.out::println);
        return true;
    }

    public Product edit(Product newProduct){
        products.forEach(product -> {
            if (product.getId().equals(newProduct.getId())) {
                product.setTitle(newProduct.getTitle());
                product.setDescription(newProduct.getDescription());
                product.setImage(newProduct.getImage());
            }
        });
        // Print the updated list of products
        products.forEach(System.out::println);
        return newProduct;
    }




}
