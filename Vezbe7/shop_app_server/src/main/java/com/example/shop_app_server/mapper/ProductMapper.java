package com.example.shop_app_server.mapper;

import com.example.shop_app_server.dto.ProductDto;
import com.example.shop_app_server.model.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductMapper {

    public Product getEntityFromDto(ProductDto dto){
        return Product.builder()
                .id(dto.getId())
                .title(dto.getTitle())
                .description(dto.getDescription())
                .image(dto.getImagePath())
                .build();
    }
    public ProductDto getDtoFromEntity(Product product){
        return ProductDto.builder()
                .id(product.getId())
                .title(product.getTitle())
                .description(product.getDescription())
                .imagePath(product.getImage())
                .build();
    }
    public List<Product> getListEntityFromListDto(List<ProductDto> productDtos){
        return productDtos.stream().map(this::getEntityFromDto).collect(Collectors.toList());
    }
    public List<ProductDto> getListDtoFromListEntity(List<Product> products){
        return products.stream().map(this::getDtoFromEntity).collect(Collectors.toList());
    }

}
