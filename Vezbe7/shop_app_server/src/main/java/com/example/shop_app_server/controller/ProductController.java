package com.example.shop_app_server.controller;

import com.example.shop_app_server.dto.ProductDto;
import com.example.shop_app_server.service.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @ApiOperation(value = "Get All Products Example", notes = "Returns all products from database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful returned products.", response = ArrayList.class)
    })
    @GetMapping
    public ResponseEntity<?> getAll(){
        System.out.println("GetAll controller method");
        return new ResponseEntity<>(productService.getAll(), HttpStatus.OK);
    }
    @ApiOperation(value = "Get Product Example", notes = "Returns product with id from database.")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful returned product.", response = ProductDto.class),
        @ApiResponse(code = 404, message = "Error", response = String.class)
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id){
        try{
            System.out.println("GetById controller method. Id : " + id);
            ProductDto productDto = productService.getById(id);
            return new ResponseEntity<>(productDto, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
    @ApiOperation(value = "Add Product Example", notes = "Add product with productDto body.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful added product.", response = ProductDto.class)
    })
    @PostMapping
    public ResponseEntity<?> add(@RequestBody ProductDto productDto){
        System.out.println("Add controller method. New product: " + productDto);
        return new ResponseEntity<>(productService.add(productDto), HttpStatus.OK);
    }
    @ApiOperation(value = "Delete Product Example", notes = "Delete product with id from database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful deleted product.", response = ProductDto.class)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") Long id){
        System.out.println("DeleteById controller method. Id : " + id);
        return new ResponseEntity<>(productService.deleteById(id), HttpStatus.OK);
    }
    @ApiOperation(value = "Edit Product Example", notes = "Edit product with productDto body.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful edited product.", response = ProductDto.class)
    })
    @PutMapping
    public ResponseEntity<?> edit(@RequestBody ProductDto productDto){
        System.out.println("Edit controller method. Product: " + productDto);
        return new ResponseEntity<>(productService.edit(productDto), HttpStatus.OK);
    }
}
