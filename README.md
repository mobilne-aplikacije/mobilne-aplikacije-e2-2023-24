# Mobilne aplikacije (2023/2024)

Materijali za vežbe na predmetu **Mobilne aplikacije (MA)** na smeru **Računarstvo i automatika**, Fakultet tehničkih nauka, Novi Sad. 

[![Ftn logo](ftn-logo.png)](http://www.ftn.uns.ac.rs/691618389/fakultet-tehnickih-nauka)

