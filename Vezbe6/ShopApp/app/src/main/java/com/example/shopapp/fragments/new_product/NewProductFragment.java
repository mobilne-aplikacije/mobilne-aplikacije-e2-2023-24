package com.example.shopapp.fragments.new_product;

import androidx.lifecycle.ViewModelProvider;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.shopapp.database.DBContentProvider;
import com.example.shopapp.database.ProductRepository;
import com.example.shopapp.database.SQLiteHelper;
import com.example.shopapp.databinding.FragmentNewProductBinding;

import java.util.Objects;

public class NewProductFragment extends Fragment {

    private NewProductViewModel mViewModel;
    private FragmentNewProductBinding binding;
    private EditText titleText;
    private EditText descrText;
    private Button confirmButton;
    public static NewProductFragment newInstance() {
        return new NewProductFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mViewModel = new ViewModelProvider(this).get(NewProductViewModel.class);

        binding = FragmentNewProductBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

//        final TextView textView = binding.productsTitle;
//        productsViewModel.getTitle().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    @Override
    public void onViewCreated(@Nullable View view, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(NewProductViewModel.class);
        titleText = binding.newTitle;
        descrText = binding.newDescr;
        confirmButton = binding.addNewPr;
        // TODO: Use the ViewModel
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewProduct();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    private void addNewProduct() {
        String title = titleText.getText().toString();
        String description = descrText.getText().toString();
// only save if either summary or description
// is available
        if (title.length() == 0 && description.length() == 0) {
            return;
        }
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_TITLE, title);
        values.put(SQLiteHelper.COLUMN_DESCRIPTION, description);
        requireActivity().getContentResolver().insert(
                    DBContentProvider.CONTENT_URI_PRODUCTS,
                    values);
//        ProductRepository productRepository = new ProductRepository(getContext());
//        productRepository.open();
//        productRepository.insertData(title, description, "image");
//        productRepository.close();

        requireActivity().getSupportFragmentManager().popBackStack();
    }


}